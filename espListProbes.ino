extern "C" {
    #include <user_interface.h>
}
#include "./espdefs.h"
#include <math.h>



/******************************
 Process 80211 frame 
*******************************/
static void processFrame( receivedPacket *packet ) {
    // Get frame control bitfield
    uint16_t frameCtrl;
    frameCtrl  = packet->data[1] << 8; 
    frameCtrl |= packet->data[0];
    
    // Get frame type/subtype 
    uint8_t frameType    = (frameCtrl & 0x0C) >> 2;
    uint8_t frameSubType = (frameCtrl & 0xF0) >> 4;

    // Only process probe requests
    if( frameType    != PKT_TYPE_MANAGEMENT ||
        frameSubType != PKT_SUBTYPE_PROBE_REQ) { 
        return;
    }
    // Get MAC address from packet data
    // Size includes a null byte - put in by snprintf
    char mac[ MAC_SIZE ];
    snprintf( mac, MAC_SIZE, "%02X:%02X:%02X:%02X:%02X:%02X",
              packet->data[ MAC_OFFSET + 0 ], packet->data[ MAC_OFFSET + 1 ],
              packet->data[ MAC_OFFSET + 2 ], packet->data[ MAC_OFFSET + 3 ],
              packet->data[ MAC_OFFSET + 4 ], packet->data[ MAC_OFFSET + 5 ] );
    Serial.print( mac );
    Serial.print( '|' );
    
    // Get SSID from packet data - Format bradcast probes
    uint8_t len = packet->data[ SSID_LEN_OFFSET ];
    if( len == 0 ) {
        Serial.print( "BROADCAST PROBE|" );
    } else {
        uint8_t start = SSID_LEN_OFFSET + 1;
        uint8_t stop  = start + len;
        for( int i=start; i<stop; i++) {
            Serial.write( packet->data[ i ] );
        }
        Serial.print( '|' );
    }

    // Get RSSI from frame header struct
    Serial.print( packet->rx_ctrl.rssi, DEC );
    Serial.print( "dBm|" );

    // Calculate approximate distance
    float exp = (-(packet->rx_ctrl.rssi)-40.09)/20;
    exp = pow(10, exp);
    Serial.print( exp );
    Serial.print( "m" );
    Serial.println();
}

/****************************
 Promisc mode event callback
*****************************/
static void ICACHE_FLASH_ATTR rxCallbackEvent(uint8_t *buffer, uint16_t length) {
    // Fill struct with buffer content
    struct receivedPacket *packet = (struct receivedPacket*) buffer;
    // Proces the received frame
    processFrame( packet );
}

/******************************
 Channel hop event callback
 Hop through channels 1-14
*******************************/
void channelHopEvent() {
    // Increment the current channel
    uint8_t channel = wifi_get_channel() + 1;
    // Wrap >14
    if ( channel > 14 ) { channel = 1; }
    // Set new channel
    wifi_set_channel( channel );
}

void setup() {
    // Setup serial interface
    Serial.begin( BAUD_RATE );
    
    // Setup WiFi interface (promiscuous)
    wifi_set_opmode( STATION_MODE );
    wifi_set_channel( 1 );
    
    // Set promiscuous mode event callback function
    wifi_promiscuous_enable( DISABLED );
    wifi_set_promiscuous_rx_cb( rxCallbackEvent );
    wifi_promiscuous_enable( ENABLED );
    
    // Set channel hop event callback
    static os_timer_t channelHop_timer;
    os_timer_disarm( &channelHop_timer );
    os_timer_setfn( &channelHop_timer, (os_timer_func_t *)channelHopEvent, NULL );
    os_timer_arm( &channelHop_timer, CH_HOP_INTERVAL_MILLIS, ENABLED );
}

void loop() {
  // put your main code here, to run repeatedly:

}
