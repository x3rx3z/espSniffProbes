A general purpose WiFi probe request sniffer created from the BSides 2018 badge.

For dev,

1. Install Arduino IDE
2. In preferences add additional board manager URL `http://arduino.esp8266.com/stable/package_esp8266com_index.json`
3. Install board libs for ESP8266 (Tools->Boards->Board Manager)
4. Set clock to 160Mhz
5. Set board type to nodeMCU 1.0