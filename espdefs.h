#ifndef ESPDEFS_H
// General
#define ENABLED                 1
#define DISABLED                0
#define BAUD_RATE               115200
// 80211 RX packet data length
#define RX_PKT_DATA_LEN         112
// 80211 RX packet types
#define PKT_TYPE_MANAGEMENT     0x00
#define PKT_TYPE_CONTROL        0x01
#define PKT_TYPE_DATA           0x02
#define PKT_SUBTYPE_PROBE_REQ   0x04
// Channel hop timer value
#define CH_HOP_INTERVAL_MILLIS  100
// Data specific defines
#define MAC_OFFSET              10
#define MAC_SIZE                18 // Includes byte delimeters and trailing null
#define SSID_LEN_OFFSET         25

/**********************************
* This absolute mess of a struct  *
* is from the technical data sheet*
**********************************/
struct rxControl {
    signed     rssi            :8;
    unsigned   rate            :4;
    unsigned   is_group        :1;
    unsigned                   :1;
    // 0:is 11n packet; 1:is not 11n packet;
    unsigned   sig_mode        :2;
    // if not 11n packet, shows length of packet.
    unsigned   legacy_length   :12; 
    unsigned   damatch0        :1;
    unsigned   damatch1        :1;
    unsigned   bssidmatch0     :1;
    unsigned   bssidmatch1     :1;
    // if is 11n packet, shows the modulation and code used (range from 0 to 76)
    unsigned   MCS             :7;
    // if is 11n packet, shows if is HT40 packet or not
    unsigned   CWB             :1;
    // if is 11n packet, shows length of packet.
    unsigned   HT_length       :16;
    unsigned   Smoothing       :1;
    unsigned   Not_Sounding    :1;
    unsigned                   :1;
    unsigned   Aggregation     :1;
    unsigned   STBC            :2;
    // if is 11n packet, shows if is LDPC packet or not.
    unsigned   FEC_CODING      :1;
    unsigned   SGI             :1;
    unsigned   rxend_state     :8;
    unsigned   ampdu_cnt       :8;
    unsigned   channel         :4;
    unsigned                   :12;
};


struct receivedPacket{
    struct   rxControl rx_ctrl;
    uint8_t  data[ RX_PKT_DATA_LEN ];
    uint16_t cnt;
    uint16_t len;
};

#endif
